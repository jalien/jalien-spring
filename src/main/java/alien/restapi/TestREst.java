package alien.restapi;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import alien.boot.JBoxConfiguration;

@RestController
@RequestMapping("/greeting")
public class TestREst {
	@Autowired
	JBoxConfiguration jBoxConfiguration;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody JAliEnRestResult greeting() {
		
		JAliEnRestResult jalien = new JAliEnRestResult();
		
		jalien.setMetaInfoEntry("port", jBoxConfiguration.getPort()+"");
		jalien.setMetaInfoEntry("user", "agrigora");
		jalien.setMetaInfoEntry("role", "admin");
		jalien.setMetaInfoEntry("pwd", "/alice/users/a/agrigora");
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("name", "/alice/users/a/agrigora/test.jdl");
		hm.put("permissions", "600");
		hm.put("owner", "agrigora");
		jalien.addResult(hm);
		
		HashMap<String, String> hm1 = new HashMap<String, String>();
		hm.put("name", "/alice/users/a/agrigora/root.jdl");
		hm.put("permissions", "777");
		hm.put("owner", "grigoras");
		jalien.addResult(hm1);
		
		HashMap<String, String> hm2 = new HashMap<String, String>();
		hm.put("name", "/alice/users/a/agrigora/test.jdl");
		hm.put("permissions", "600");
		hm.put("owner", "agrigora");
		jalien.addResult(hm2);
		
		return jalien;
	}
}

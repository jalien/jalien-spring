package alien.restapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Alina Grigoras
 * */
public class JAliEnRestResult {
	private final Map<String, String> metaInfo = new TreeMap<String, String>();
	private final List<Map<String, String>> results = new ArrayList<>();
	
	/**
	 * Adding key/value to the meta information
	 * @param sKey name of the meta information
	 * @param sValue value of the meta information
	 */
	public void setMetaInfoEntry(final String sKey, final String sValue){
		metaInfo.put(sKey, sValue);
	}
	
	/**
	 * Retrieve meta infomation by key
	 * @param sKey name of the requested meta information
	 */
	public String getMetaInfoEntry(final String sKey){
		if(metaInfo.containsKey(sKey))
			return metaInfo.get(sKey);
		else{
			throw new NullPointerException("The meta hash does not contain any key with this value"); 
		}
	}
	
	/**
	 * @param result The result line that will be added to the result list
	 */
	public void addResult(final Map<String, String> result){
		results.add(result);
	}
	
	public Map<String, String> getMetaInfo() {
		return metaInfo;
	}
	public List<Map<String, String>> getResults() {
		return results;
	}
	
	
}

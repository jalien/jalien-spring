package alien;

import alien.site.ComputingElement;


/**
 * @author ron
 *  @since Jun 05, 2011
 */
public class SiteCE {
	
	
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		ComputingElement ce = new  ComputingElement();
		ce.start();

	}
}

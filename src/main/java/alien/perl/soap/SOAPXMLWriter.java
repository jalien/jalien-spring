package alien.perl.soap;

/**
 * @author Alina Grigoras
 * 
 */
public interface SOAPXMLWriter {

	/**
	 * @return SOAP encoded object
	 */
	public String toSOAPXML();

}

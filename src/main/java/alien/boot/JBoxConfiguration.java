package alien.boot;

import java.util.Iterator;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

/**
 * Spring Boot JBox start up class
 * @author Alina Grigoras
 *
 */
@SpringBootApplication
@ComponentScan("alien")
public class JBoxConfiguration {
	private static final Log log = LogFactory.getLog(JBoxConfiguration.class);

	@Autowired
	public Environment environment;

	/**
	 * HTTP running port return by Tomcat
	 */
	private int port;

	/**
	 * Token password
	 */
	private String password;

	/**
	 * @return
	 */
	public static JBoxConfiguration startJBoxDeamon(int port){
		System.setProperty("server.port", port+"");
		
		SpringApplication springApp = new SpringApplication(JBoxConfiguration.class);

		// Adding the ServletContainer event in order to grab the running port
		EmbededServerPortRetriver pr = new EmbededServerPortRetriver();
		springApp.addListeners(pr);

		// Run the Boot application
		ConfigurableApplicationContext appContext = springApp.run();

		JBoxConfiguration jBox = appContext.getBean(JBoxConfiguration.class);
		jBox.port = pr.getPort();
		jBox.password = generatePassword();
		
		ConfigurableListableBeanFactory bubu = appContext.getBeanFactory();
		Iterator<String> it = bubu.getBeanNamesIterator();

		while(it.hasNext()){
			log.debug("After Tomcat start, bean = " + it.next());
		}

		return jBox;
	}

	public String getPassword() {
		return password;
	}

	public int getPort() {
		return port;
	}
	
	/**
	 * @return UUID generated password
	 */
	public static String generatePassword(){
		return UUID.randomUUID().toString();
	}

	public static void main(String[] args) {
		JBoxConfiguration jBox1 = JBoxConfiguration.startJBoxDeamon(8080);
		log.debug("jbox1 = " + jBox1.getPort());
		
	}
}

package alien.boot;

import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationListener;

/**
 * Discover HTTP port at runtime using an ApplicationListener on EmbeddedServletContainerInitializedEvent
 * @author Alina Grigoras
 *
 */
public class EmbededServerPortRetriver implements ApplicationListener<EmbeddedServletContainerInitializedEvent>{
	private int port;
	
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {	
		port = event.getEmbeddedServletContainer().getPort();
	}
	
	/**
	 * @return Return the Tomcat running port
	 */
	public int getPort(){
		return port;
	}
}
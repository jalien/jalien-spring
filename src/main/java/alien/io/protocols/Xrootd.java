/**
 * 
 */
package alien.io.protocols;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import lazyj.Format;
import lia.util.process.ExternalProcess;
import lia.util.process.ExternalProcess.ExitStatus;
import lia.util.process.ExternalProcessBuilder;
import utils.ExternalCalls;
import alien.catalogue.GUID;
import alien.catalogue.PFN;
import alien.catalogue.access.AccessType;
import alien.config.ConfigUtils;
import alien.io.IOUtils;
import alien.se.SE;

/**
 * @author costing
 * @since Dec 8, 2010
 */
public class Xrootd extends Protocol {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7860814883144320429L;

	/**
	 * Logger
	 */
	static transient final Logger logger = ConfigUtils.getLogger(Xrootd.class.getCanonicalName());

	private static String xrdcpdebug = "-d";
	private int xrdcpdebuglevel = 0;

	private static String xrootd_default_path = null;

	private static String xrdcpPath = null;

	protected static boolean xrootdNewerThan4 = false;

	static {
		if (ConfigUtils.getConfig() != null) {
			xrootd_default_path = ConfigUtils.getConfig().gets("xrootd.location", null);

			if (xrootd_default_path != null) {
				for (final String command : new String[] { "xrdcpapmon", "xrdcp" }) {
					final File test = new File(xrootd_default_path + "/bin/" + command);

					if (test.exists() && test.isFile() && test.canExecute()) {
						xrdcpPath = test.getAbsolutePath();
						break;
					}
				}
			}
		}

		if (xrdcpPath == null) {
			for (final String command : new String[] { "xrdcpapmon", "xrdcp" }) {
				xrdcpPath = ExternalCalls.programExistsInPath(command);

				if (xrdcpPath != null) {
					int idx = xrdcpPath.lastIndexOf('/');

					if (idx > 0) {
						idx = xrdcpPath.lastIndexOf('/', idx - 1);

						if (idx >= 0)
							xrootd_default_path = xrdcpPath.substring(0, idx);
					}

					break;
				}
			}
		}

		if (xrdcpPath != null) {
			final ExternalProcessBuilder pBuilder = new ExternalProcessBuilder(Arrays.asList(xrdcpPath, "--version"));

			checkLibraryPath(pBuilder);

			pBuilder.returnOutputOnExit(true);

			pBuilder.timeout(15, TimeUnit.SECONDS);

			pBuilder.redirectErrorStream(true);

			final ExitStatus exitStatus;

			ExternalProcess p = null;

			try {
				p = pBuilder.start();

				if (p != null) {
					exitStatus = p.waitFor();

					if (exitStatus.getExtProcExitStatus() == 0) {
						final String version = exitStatus.getStdOut();

						logger.log(Level.FINE, "Local Xrootd version is " + version);

						if (version.indexOf('.') > 0) {
							xrootdNewerThan4 = version.substring(0, version.indexOf('.')).compareTo("v4") >= 0;
						}
					}
				} else
					logger.log(Level.WARNING, "Cannot execute " + xrdcpPath);
			} catch (final IOException | InterruptedException ie) {
				if (p != null)
					p.destroy();

				logger.log(Level.WARNING, "Interrupted while waiting for `" + xrdcpPath + " --version` to finish");
			}
		}
	}

	private static String DIFirstConnectMaxCnt = "2";

	private int timeout = 60;

	// last value must be 0 for a clean exit
	private static final int statRetryTimesXrootd[] = { 1, 5, 10, 0 };
	private static final int statRetryTimesDCache[] = { 5, 10, 15, 20, 20, 20, 30, 30, 30, 30, 0 };

	/**
	 * package protected
	 */
	public Xrootd() {
		// package protected
	}

	private static void checkLibraryPath(final ExternalProcessBuilder p) {
		if (xrootd_default_path != null) {
			String ldpath = "";
			if (p.environment().containsKey("LD_LIBRARY_PATH"))
				ldpath = p.environment().get("LD_LIBRARY_PATH");

			p.environment().put("LD_LIBRARY_PATH", ldpath + ":" + xrootd_default_path + "/lib");
		}
	}

	/**
	 * @param level
	 *            xrdcp debug level
	 */
	public void setDebugLevel(final int level) {
		xrdcpdebuglevel = level;
	}

	/**
	 * Set the xrdcp timeout
	 * 
	 * @param seconds
	 */
	public void setTimeout(final int seconds) {
		timeout = seconds;
	}

	/**
	 * Extract the most relevant failure reason from an xrdcp / xrd3cp output
	 * 
	 * @param message
	 * @return relevant portion of the output
	 */
	public static final String parseXrootdError(final String message) {
		if (message == null || message.length() == 0)
			return null;

		int idx = message.indexOf("Last server error");

		if (idx >= 0) {
			idx = message.indexOf("('", idx);

			if (idx > 0) {
				idx += 2;

				final int idx2 = message.indexOf("')", idx);

				if (idx2 > idx)
					return message.substring(idx, idx2);
			}
		}

		idx = message.lastIndexOf("\tretc=");

		if (idx >= 0) {
			int idx2 = message.indexOf('\n', idx);

			if (idx2 < 0)
				idx2 = message.length();

			return message.substring(idx + 1, idx2);
		}

		return null;
	}

	@Override
	public boolean delete(final PFN pfn) throws IOException {
		if (pfn == null || pfn.ticket == null || pfn.ticket.type != AccessType.DELETE)
			throw new IOException("You didn't get the rights to delete this PFN");

		try {
			final List<String> command = new LinkedList<>();

			// command.addAll(getCommonArguments());

			String envelope = null;

			if (pfn.ticket.envelope != null) {
				envelope = pfn.ticket.envelope.getEncryptedEnvelope();

				if (envelope == null)
					envelope = pfn.ticket.envelope.getSignedEnvelope();
			}

			File fAuthz = null;

			if (xrootdNewerThan4) {
				final String qProt = pfn.getPFN().substring(7);
				final String host = qProt.substring(0, qProt.indexOf(':'));
				final String port = qProt.substring(qProt.indexOf(':') + 1, qProt.indexOf('/'));

				command.add(xrootd_default_path + "/bin/xrdfs");
				command.add(host + ":" + port);
				command.add("rm");
				command.add(qProt.substring(qProt.indexOf('/') + 1) + "?authz=" + Format.encode(envelope));
			} else {
				command.add(xrootd_default_path + "/bin/xrdrm");
				command.add("-v");

				String transactionURL = pfn.pfn;

				if (pfn.ticket.envelope != null)
					transactionURL = pfn.ticket.envelope.getTransactionURL();

				if (envelope != null) {
					fAuthz = File.createTempFile("xrdrm-", ".authz", IOUtils.getTemporaryDirectory());

					final FileWriter fw = new FileWriter(fAuthz);

					fw.write(envelope);

					fw.flush();
					fw.close();

					command.add("-authz");
					command.add(fAuthz.getCanonicalPath());
				}

				command.add(transactionURL);
			}
			
			if (logger.isLoggable(Level.FINEST))
				logger.log(Level.FINEST, "Executing rm command: " + command);

			final ExternalProcessBuilder pBuilder = new ExternalProcessBuilder(command);

			checkLibraryPath(pBuilder);

			pBuilder.returnOutputOnExit(true);

			pBuilder.timeout(1, TimeUnit.MINUTES);

			pBuilder.redirectErrorStream(true);

			final ExitStatus exitStatus;

			try {
				exitStatus = pBuilder.start().waitFor();
			} catch (final InterruptedException ie) {
				throw new IOException("Interrupted while waiting for the following command to finish : " + command.toString());
			} finally {
				if (fAuthz != null)
					if (!fAuthz.delete())
						logger.log(Level.WARNING, "Could not delete temporary auth token file: " + fAuthz.getAbsolutePath());
			}

			if (exitStatus.getExtProcExitStatus() != 0) {
				if (logger.isLoggable(Level.FINE))
					logger.log(Level.FINE, "Exit code " + exitStatus.getExtProcExitStatus() + " and output is\n" + exitStatus.getStdOut());

				throw new IOException("Exit code " + exitStatus.getExtProcExitStatus());
			}

			if (logger.isLoggable(Level.FINEST))
				logger.log(Level.FINEST, "Exit code was zero and the output was:\n" + exitStatus.getStdOut());

			return true;
		} catch (final IOException ioe) {
			throw ioe;
		} catch (final Throwable t) {
			logger.log(Level.WARNING, "Caught exception", t);

			throw new IOException("delete aborted because " + t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alien.io.protocols.Protocol#get(alien.catalogue.PFN, alien.catalogue.access.CatalogueReadAccess, java.lang.String)
	 */
	@Override
	public File get(final PFN pfn, final File localFile) throws IOException {
		File target = null;

		if (localFile != null) {
			if (localFile.exists())
				throw new SourceException("Local file " + localFile.getCanonicalPath() + " exists already. Xrdcp would fail.");
			target = localFile;
		}

		final GUID guid = pfn.getGuid();

		if (target == null) {
			// we are free to use any cached value
			target = TempFileManager.getAny(guid);

			if (target != null) {
				logger.log(Level.FINE, "Reusing cached file: " + target.getCanonicalPath());

				return target;
			}

			target = File.createTempFile("xrootd-get", null, IOUtils.getTemporaryDirectory());

			if (!target.delete()) {
				logger.log(Level.WARNING, "Could not delete the just created temporary file: " + target);
				return null;
			}
		}

		if (pfn.ticket == null || pfn.ticket.type != AccessType.READ)
			if (logger.isLoggable(Level.FINE))
				logger.log(Level.FINE, "The envelope for PFN " + pfn.toString() + (pfn.ticket == null ? " could not be found" : " is not a READ one"));

		try {
			final List<String> command = new LinkedList<>();

			if (xrdcpPath == null) {
				logger.log(Level.SEVERE, "Could not find xrdcp in path.");
				throw new SourceException("Could not find xrdcp in path.");
			}

			command.add(xrdcpPath);

			command.addAll(getCommonArguments());

			/*
			 * TODO: enable when servers support checksum queries, at the moment most don't if (xrootdNewerThan4 && guid.md5 != null && guid.md5.length() > 0) { command.add("-C"); command.add("md5:" +
			 * guid.md5); }
			 */

			String transactionURL = pfn.pfn;

			if (pfn.ticket != null && pfn.ticket.envelope != null)
				transactionURL = pfn.ticket.envelope.getTransactionURL();

			if (pfn.ticket != null && pfn.ticket.envelope != null)
				if (pfn.ticket.envelope.getEncryptedEnvelope() != null)
					command.add("-OS&authz=" + pfn.ticket.envelope.getEncryptedEnvelope());
				else if (pfn.ticket.envelope.getSignedEnvelope() != null)
					command.add("-OS" + pfn.ticket.envelope.getSignedEnvelope());

			command.add(transactionURL);
			command.add(target.getCanonicalPath());

			final ExternalProcessBuilder pBuilder = new ExternalProcessBuilder(command);

			checkLibraryPath(pBuilder);

			pBuilder.returnOutputOnExit(true);

			// 20KB/s should be available to anybody
			long maxTime = guid.size / 20000;

			maxTime += timeout;

			pBuilder.timeout(maxTime, TimeUnit.SECONDS);

			pBuilder.redirectErrorStream(true);

			final ExitStatus exitStatus;

			ExternalProcess p = null;

			try {
				p = pBuilder.start();

				if (p != null)
					exitStatus = p.waitFor();
				else
					throw new SourceException("Cannot start the process");
			} catch (final InterruptedException ie) {
				p.destroy();

				throw new SourceException("Interrupted while waiting for the following command to finish : " + command.toString());
			}

			if (exitStatus.getExtProcExitStatus() != 0) {
				String sMessage = parseXrootdError(exitStatus.getStdOut());

				logger.log(Level.WARNING, "GET of " + pfn.pfn + " failed with " + exitStatus.getStdOut());

				if (sMessage != null)
					sMessage = xrdcpPath + " exited with " + exitStatus.getExtProcExitStatus() + ": " + sMessage;
				else
					sMessage = "Exit code was " + exitStatus.getExtProcExitStatus() + " for command : " + command.toString();

				throw new SourceException(sMessage);
			}

			if (!checkDownloadedFile(target, pfn))
				throw new SourceException("Local file doesn't match catalogue details (" + (target.exists() ? "" + target.length() : "n/a") + " vs " + guid.size + ")");
		} catch (final SourceException ioe) {
			if (target.exists() && !target.delete())
				logger.log(Level.WARNING, "Could not delete temporary file on IO exception: " + target);
			else {
				// make sure it doesn't pop up later after an interrupt
				TempFileManager.putTemp(alien.catalogue.GUIDUtils.createGuid(), target);
				TempFileManager.release(target);
			}

			throw ioe;
		} catch (final Throwable t) {
			if (target.exists() && !target.delete())
				logger.log(Level.WARNING, "Could not delete temporary file on throwable: " + target);
			else {
				// make sure it doesn't pop up later after an interrupt
				TempFileManager.putTemp(alien.catalogue.GUIDUtils.createGuid(), target);
				TempFileManager.release(target);
			}

			logger.log(Level.WARNING, "Caught exception", t);

			throw new SourceException("Get aborted because " + t);
		}

		if (localFile == null)
			TempFileManager.putTemp(guid, target);
		else
			TempFileManager.putPersistent(guid, target);

		return target;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alien.io.protocols.Protocol#put(alien.catalogue.PFN, alien.catalogue.access.CatalogueWriteAccess, java.lang.String)
	 */
	@Override
	public String put(final PFN pfn, final File localFile) throws IOException {
		if (localFile == null || !localFile.exists() || !localFile.isFile() || !localFile.canRead())
			throw new TargetException("Local file " + localFile + " cannot be read");

		if (pfn.ticket == null || pfn.ticket.type != AccessType.WRITE)
			throw new TargetException("No access to this PFN");

		final GUID guid = pfn.getGuid();

		if (localFile.length() != guid.size)
			throw new TargetException("Difference in sizes: local=" + localFile.length() + " / pfn=" + guid.size);

		try {
			final List<String> command = new LinkedList<>();

			if (xrdcpPath == null) {
				logger.log(Level.SEVERE, "Could not find xrdcp in path.");
				throw new TargetException("Could not find xrdcp in path.");
			}

			command.add(xrdcpPath);

			command.addAll(getCommonArguments());

			command.add("-np"); // no progress bar
			command.add("-v"); // display summary output
			command.add("-f"); // re-create a file if already present
			command.add("-P"); // request POSC (persist-on-successful-close) processing to create a new file

			/*
			 * TODO: enable when storages support checksum queries, at the moment most don't if (xrootdNewerThan4 && guid.md5!=null && guid.md5.length()>0){ command.add("-C");
			 * command.add("md5:"+guid.md5); }
			 */

			command.add(localFile.getCanonicalPath());

			String transactionURL = pfn.pfn;

			if (pfn.ticket.envelope != null)
				transactionURL = pfn.ticket.envelope.getTransactionURL();

			if (pfn.ticket.envelope != null)
				if (pfn.ticket.envelope.getEncryptedEnvelope() != null)
					command.add("-OD&authz=" + pfn.ticket.envelope.getEncryptedEnvelope());
				else if (pfn.ticket.envelope.getSignedEnvelope() != null)
					command.add("-OD" + pfn.ticket.envelope.getSignedEnvelope());

			command.add(transactionURL);

			final ExternalProcessBuilder pBuilder = new ExternalProcessBuilder(command);

			checkLibraryPath(pBuilder);

			pBuilder.returnOutputOnExit(true);

			// 20KB/s should be available to anybody
			final long maxTime = timeout + guid.size / 20000;

			pBuilder.timeout(maxTime, TimeUnit.SECONDS);

			pBuilder.redirectErrorStream(true);

			final ExitStatus exitStatus;

			try {
				exitStatus = pBuilder.start().waitFor();
			} catch (final InterruptedException ie) {
				throw new TargetException("Interrupted while waiting for the following command to finish : " + command.toString());
			}

			if (exitStatus.getExtProcExitStatus() != 0) {
				String sMessage = parseXrootdError(exitStatus.getStdOut());

				logger.log(Level.WARNING, "PUT of " + pfn.pfn + " failed with " + exitStatus.getStdOut());

				if (sMessage != null)
					sMessage = xrdcpPath + " exited with " + exitStatus.getExtProcExitStatus() + ": " + sMessage;
				else
					sMessage = "Exit code was " + exitStatus.getExtProcExitStatus() + " for command : " + command.toString();

				throw new TargetException(sMessage);
			}

			if (pfn.ticket.envelope.getEncryptedEnvelope() != null)
				return xrdstat(pfn, false);

			return xrdstat(pfn, true);
		} catch (final TargetException ioe) {
			throw ioe;
		} catch (final IOException ioe) {
			throw new TargetException(ioe.getMessage());
		} catch (final Throwable t) {
			logger.log(Level.WARNING, "Caught exception", t);

			throw new TargetException("Put aborted because " + t);
		}
	}

	private final List<String> getCommonArguments() {
		final List<String> ret = new ArrayList<>();

		ret.add("-DIFirstConnectMaxCnt");
		ret.add(DIFirstConnectMaxCnt);

		if (xrdcpdebuglevel > 0) {
			ret.add(xrdcpdebug);
			ret.add(String.valueOf(xrdcpdebuglevel));
		}

		if (timeout > 0) {
			ret.add("-DITransactionTimeout");
			ret.add(String.valueOf(timeout));

			ret.add("-DIRequestTimeout");
			ret.add(String.valueOf(timeout));
		}

		ret.add("-DIReadCacheSize");
		ret.add("0");

		return ret;
	}

	/**
	 * Check if the PFN has the correct properties, such as described in the access envelope
	 * 
	 * @param pfn
	 * @param returnEnvelope
	 * @return the signed envelope from the storage, if it knows how to generate one
	 * @throws IOException
	 *             if the remote file properties are not what is expected
	 */
	public String xrdstat(final PFN pfn, final boolean returnEnvelope) throws IOException {
		return xrdstat(pfn, returnEnvelope, true, false);
	}

	/**
	 * @param output
	 * @return the command output less some of the irrelevant messages
	 */
	static String cleanupXrdOutput(final String output) {
		final StringBuilder sb = new StringBuilder(output.length());

		final BufferedReader br = new BufferedReader(new StringReader(output));

		String line;

		try {
			while ((line = br.readLine()) != null)
				if (!line.startsWith("Overriding '"))
					sb.append(line).append('\n');
		} catch (final IOException ioe) {
			// ignore, cannot happen
		}

		return sb.toString().replaceAll("[\\n\\r\\s]+$", "");
	}

	/**
	 * Check if the PFN has the correct properties, such as described in the access envelope
	 * 
	 * @param pfn
	 * @param returnEnvelope
	 * @param retryWithDelay
	 * @param forceRecalcMd5
	 * @return the signed envelope from the storage, if it knows how to generate one
	 * @throws IOException
	 *             if the remote file properties are not what is expected
	 */
	public String xrdstat(final PFN pfn, final boolean returnEnvelope, final boolean retryWithDelay, final boolean forceRecalcMd5) throws IOException {

		final SE se = pfn.getSE();

		if (se == null)
			throw new IOException("SE " + pfn.seNumber + " doesn't exist");

		final int[] statRetryTimes = se.seName.toLowerCase().contains("dcache") ? statRetryTimesDCache : statRetryTimesXrootd;

		for (int statRetryCounter = 0; statRetryCounter < statRetryTimes.length; statRetryCounter++)
			try {
				final List<String> command = new LinkedList<>();

				final String qProt = pfn.getPFN().substring(7);
				final String host = qProt.substring(0, qProt.indexOf(':'));
				final String port = qProt.substring(qProt.indexOf(':') + 1, qProt.indexOf('/'));

				if (xrootdNewerThan4) {
					command.add(xrootd_default_path + "/bin/xrdfs");
					command.add(host + ":" + port);
					command.add("stat");
					command.add(qProt.substring(qProt.indexOf('/') + 1));
				} else {
					if (returnEnvelope) {
						// xrd pcaliense01:1095 query 32 /15/63447/e3f01fd2-23e3-11e0-9a96-001f29eb8b98?getrespenv=1\&recomputemd5=1
						command.add(xrootd_default_path + "/bin/xrd");

						command.add(host + ":" + port);
						command.add("query");
						command.add("32");
						String qpfn = qProt.substring(qProt.indexOf('/') + 1) + "?getrespenv=1";

						if (forceRecalcMd5)
							qpfn += "\\&recomputemd5=1";

						command.add(qpfn);
					} else {
						command.add(xrootd_default_path + "/bin/xrdstat");
						command.addAll(getCommonArguments());
						command.add(pfn.getPFN());
					}
				}

				final ExternalProcessBuilder pBuilder = new ExternalProcessBuilder(command);

				checkLibraryPath(pBuilder);

				pBuilder.returnOutputOnExit(true);

				pBuilder.timeout(15, TimeUnit.SECONDS);

				pBuilder.redirectErrorStream(true);

				ExitStatus exitStatus;

				try {
					exitStatus = pBuilder.start().waitFor();
				} catch (final InterruptedException ie) {
					throw new IOException("Interrupted while waiting for the following command to finish : " + command.toString());
				}

				final int sleep = statRetryTimes[statRetryCounter];

				if (exitStatus.getExtProcExitStatus() != 0) {
					if (sleep == 0 || !retryWithDelay)
						throw new IOException("Exit code was " + exitStatus.getExtProcExitStatus() + ", retry #" + (statRetryCounter + 1) + ", output was " + cleanupXrdOutput(exitStatus.getStdOut())
								+ ", " + "for command : " + command.toString());

					Thread.sleep(sleep * 1000);
					continue;
				}

				if (returnEnvelope)
					return cleanupXrdOutput(exitStatus.getStdOut());

				final long filesize = checkOldOutputOnSize(exitStatus.getStdOut());

				if (pfn.getGuid().size == filesize)
					return cleanupXrdOutput(exitStatus.getStdOut());

				if (sleep == 0 || !retryWithDelay)
					throw new IOException(command.toString() + ": could not confirm the upload after " + (statRetryCounter + 1) + " retries: " + cleanupXrdOutput(exitStatus.getStdOut()));

				Thread.sleep(sleep * 1000);
				continue;

			} catch (final IOException ioe) {
				throw ioe;
			} catch (final Throwable t) {
				logger.log(Level.WARNING, "Caught exception", t);

				final IOException ioe = new IOException("xrdstat internal failure " + t);

				ioe.setStackTrace(t.getStackTrace());

				throw ioe;
			}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see alien.io.protocols.Protocol#transfer(alien.catalogue.PFN, alien.catalogue.access.CatalogueReadAccess, alien.catalogue.PFN, alien.catalogue.access.CatalogueWriteAccess)
	 */
	@Override
	public String transfer(final PFN source, final PFN target) throws IOException {
		final File temp = get(source, null);

		try {
			return put(target, temp);
		} finally {
			TempFileManager.release(temp);
		}
	}

	private static long checkOldOutputOnSize(final String stdout) {
		long size = 0;
		String line = null;
		final BufferedReader reader = new BufferedReader(new StringReader(stdout));

		try {
			while ((line = reader.readLine()) != null) {
				if (xrootdNewerThan4) {
					if (line.startsWith("Size:")) {
						size = Long.parseLong(line.substring(line.lastIndexOf(':') + 1).trim());
						break;
					}
				} else if (line.startsWith("xstat:")) {
					final int idx = line.indexOf("size=");

					if (idx > 0) {
						final int idx2 = line.indexOf(" ", idx);

						size = Long.parseLong(line.substring(idx + 5, idx2));

						break;
					}
				}
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "xrootd";
	}

	@Override
	int getPreference() {
		return 2;
	}

}
